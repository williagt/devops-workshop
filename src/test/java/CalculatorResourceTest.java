import org.junit.Test;
import org.junit.jupiter.api.Nested;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CalculatorResourceTest {




        @Test
        public void testCalculateManyOperations(){
            CalculatorResource calculatorResource = new CalculatorResource();

            String expression = "100+400+33";
            assertEquals(533, calculatorResource.calculate(expression));

            expression = "100-400-33";
            assertEquals(-333, calculatorResource.calculate(expression));

            expression = "2*10*33";
            assertEquals(660, calculatorResource.calculate(expression));

            expression = "100/10/10";
            assertEquals(1, calculatorResource.calculate(expression));

            try{
                expression = "10s+2";
                calculatorResource.calculate(expression);
            }catch (IllegalArgumentException e){
                assertTrue(true);
            }
        }
        @Test
        public void testCalculate() {
            CalculatorResource calculatorResource = new CalculatorResource();

            //Sum
            String expression = "100+300";
            assertEquals(400, calculatorResource.calculate(expression));

            //Subtraction
            expression = " 300 - 99 ";
            assertEquals(201, calculatorResource.calculate(expression));

            //Multiplication
            expression = "3 * 9";
            assertEquals(27, calculatorResource.calculate(expression));

            //Division
            expression = "6/3";
            assertEquals(2, calculatorResource.calculate(expression));
        }




    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "10*10";
        assertEquals(100, calculatorResource.multiplication(expression));

        expression = "7*9";

        assertEquals(63, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "9/3";
        assertEquals(3, calculatorResource.division(expression));

        expression = "27/9";
        assertEquals(3, calculatorResource.division(expression));
    }
}
